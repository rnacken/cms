<?php
define( "DEVELOPMENT_ENVIRONMENT", true );

date_default_timezone_set( "Europe/Amsterdam" );  // http://www.php.net/manual/en/timezones.php

define( "DB_DSN", "mysql:host=localhost;dbname=cms_db" );
define( "DB_USERNAME", "root" );
define( "DB_PASSWORD", "root" );
define( "ADMIN_USERNAME", "admin" );
define( "ADMIN_PASSWORD", "mypass" );

define( "SITE_URL", "http://cms.localhost/api/");
//require( CLASS_PATH . "/Article.php" );

//var_dump(json_encode(array('maxlength'=>10)));


function handleException( $exception ) {
    // quick & dirty
    echo "<pre>ERROR: ".$exception->getMessage()."<br />";
    $trace = $exception->getTrace();
    if ($trace && is_array($trace)){
        foreach($trace as $step){
            echo issetOr($step['function'], 'direct').' ('.issetOr($step['line'], 'x').') - '.issetOr($step['file'], 'direct call').'<br />';
        }
    }
    echo "</pre>";
    error_log( $exception->getMessage() );
    exit();
}

set_exception_handler( 'handleException' );
?>