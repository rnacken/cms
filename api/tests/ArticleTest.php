<?php

class ArticleTest extends PHPUnit_Framework_TestCase {

    public function testNewArticle(){
        $test_art = new Article();
        $this->assertEquals(null, $test_art->id);
    }
    public function testNewArticle2(){
        $test_art = new Article(array(
            'id' => '10',
            'type' => 'site'
        ));
        $this->assertEquals(10, $test_art->id);
    }

}
