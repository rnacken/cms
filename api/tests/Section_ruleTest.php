<?php

class Section_ruleTest extends PHPUnit_Framework_TestCase {

    public function testCompareType(){
        $sec_rule = new Section_rule();
        $test_rule_group = array(
            array('id' => '1',
                'section_id' => '1',
                'param' => 'type',
                'operator' => '==',
                'value' => 'site',
                'rule_group' => '10'
            )
        );
        $test_art = new Article(array(
            'id' => '10',
            'type' => 'site'
        ));
        $result = $sec_rule->validateRuleGroup($test_rule_group, $test_art);
        $this->assertEquals(true, $result);
    }
    public function testCompareTypeFail(){
        $sec_rule = new Section_rule();
        $test_rule_group = array(
            array('id' => '1',
                'section_id' => '1',
                'param' => 'type',
                'operator' => '==',
                'value' => 'page',      // wrong
                'rule_group' => '10'
            )
        );
        $test_art = new Article(array(
            'id' => '10',
            'type' => 'site'
        ));
        $result = $sec_rule->validateRuleGroup($test_rule_group, $test_art);
        $this->assertEquals(false, $result);
    }
    /**
     * @dataProvider validateRuleGroupProvider
     */
    public function testValidateRuleGroup1($test_rule_group, $test_art){
        // test group with 2 checks
        $sec_rule = new Section_rule();
        $result = $sec_rule->validateRuleGroup($test_rule_group, $test_art);
        $this->assertEquals(true, $result);
    }

    public function validateRuleGroupProvider(){
        $test_art = new Article(array(
            'id' => '10',
            'type' => 'site'
        ));
        $arrangement = array(
            array(
                array(
                    array('id' => '1',
                        'section_id' => '1',
                        'param' => 'type',
                        'operator' => '==',
                        'value' => 'site',
                        'rule_group' => '10'
                    ),
                    array('id' => '2',
                        'section_id' => '1',
                        'param' => 'type',
                        'operator' => '==',
                        'value' => 'site',
                        'rule_group' => '10'
                    )
                ),
                $test_art
            )
        );
        return $arrangement;
    }

    public function testGetSections1(){
        // test 2 groups, one with 2 checks - succeed on first
        $sec_rule = new Section_rule();
//        $test_sec_rule_group = array('sec10' => array(
//            'group1' => array(
//                array(
//                    array('id' => '1',
//                        'section_id' => 'sec10',
//                        'param' => 'type',
//                        'operator' => '==',
//                        'value' => 'site',
//                        'rule_group' => '10'
//                    ),
//                    array('id' => '2',
//                        'section_id' => 'sec10',
//                        'param' => 'id',
//                        'operator' => '==',
//                        'value' => '10',
//                        'rule_group' => '10'
//                    )
//                )
//            ),
//            'group2' => array(
//                array(
//                    array('id' => '1',
//                        'section_id' => 'sec10',
//                        'param' => 'type',
//                        'operator' => '==',
//                        'value' => 'site',
//                        'rule_group' => '10'
//                    )
//                )
//            )
//        ));
        $test_sections = array('sec10', 'sec20');
        $test_art = new Article(array(
            'id' => '10',
            'type' => 'site'
        ));
        $result = array(); //$sec_rule->getSections($test_sections, $test_art);
        $this->assertEquals(array(), $result);
    }

}
