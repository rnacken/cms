'use stric()';

module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('grunt-cms.json'),
        // global
        documentRoot: 'www',
        bower: 'bower_components',
        // Task configuration.
        /*jshint: {
            options: {
                reporter: require('jshint-stylish')
            },
            gruntfile: {
                options: {
                    jshintrc: '.jshintrc'
                },
                src: 'Gruntfile.js'
            },
            dev: {
                options: {
                    jshintrc: '.jshintrc'
                },
                src: [
                    '<%= pkg.main %>js/*.js'
                ]
            },
            production: {
                options: {
                    jshintrc: '.jshintrc'
                },
                src: [
                    '<%= pkg.main %>js/*.js'
                ]
            }
        },
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true,
                separator: ';'
            },
            development: {
                src: [  "<%= pkg.main %>less/*.less"
                ],
                dest: "<%= pkg.main %>less/.all.less"
            }
        },
        less: {
            options: {
                paths: [
                    '<%= css %>',
                    '<%= bower %>'
                ],
                relativeUrls: true
            },
            dev: {
                options: {
                    sourceMap: false,
                    compress: false
                },
                files: {
                    "<%= pkg.main %>css/style.css": "<%= pkg.main %>less/.all.less"
                }
            },
            production: {
                options: {
                    sourceMap: false,
                    compress: false
                },
                files: {
                    "<%= pkg.main %>css/style.css": "<%= pkg.main %>less/.all.less"
                }
            }
        },*/
        watch: {
            options: {
                atBegin: true
            },
            /*gruntfile: {
                files: 'Gruntfile.js',
                tasks: ['jshint:gruntfile']
            },*/
            phptests: {
                files: ['./application/controllers/*',
                    './application/models/*',
                    './application/views/*',
                    './tests/*'],
                tasks: ['phpunit']
            }
            /*scripts: {
                files: ['<%= pkg.main %>js/*'],
                tasks: ['jshint']
            },
            less: {
                files: ['<%= pkg.main %>less/*.less'],
                tasks: ['concat', 'less']  
            },*/
        },
        phpunit: {
            classes: {
                dir: 'tests/'
            },
            options: {
                colors: true,
                bin: '/usr/local/bin/phpunit',
                bootstrap: 'tests/bootstrap.php'
            }
        }
    });

    /*grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');*/
    grunt.loadNpmTasks('grunt-phpunit');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task.
    grunt.registerTask('default', ['watch']);

};
