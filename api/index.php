<?php

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(dirname(__FILE__)));
define('PARTIALS_DIR', '_partials');

define('RESPONSE', 'json');

if (isset($_GET['url'])){
    $url = $_GET['url'];
}

require_once (ROOT . DS . 'library' . DS . 'bootstrap.php');
