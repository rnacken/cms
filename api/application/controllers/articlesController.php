<?php

class ArticlesController extends Controller {

    function view($id = null) {
        $art = $this->Article->getArticleById($id);
        $fieldset = new Fieldset();
        $field_value = new Field_value();
        $art->fieldsets = $fieldset->getFieldsets($art);
        foreach($art->fieldsets as &$fieldset){
            $fieldset->fields = $fieldset->getFieldsetFields($fieldset->id, 0, $art);
            foreach($fieldset->fields as $field){
                $art->values[$field->name] = $field_value->getFieldValue($id, $field);
            }
        }
        $this->set('response', $art);
    }

    function edit($id = null) {
        $art = $this->Article->getArticleById($id); // old data
        // todo: show alternative if no art is found

        if(isset($_POST['submit'])){
            $art->saveForm($_POST);
        }
        $fieldset = new Fieldset();
        $art->fieldsets = $fieldset->getFieldsets($art);
        $this->set('response', $art);
    }



}