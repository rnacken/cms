<?php

class FieldsController extends Controller {

    function edit($id = null) {
        $field = $this->Field->getFieldById($id); // old data
        // todo: show alternative if no field is found
        if(isset($_POST['submit'])){
            echo 'saved!';
            $field->saveForm($_POST);
        } else {
            echo 'editing mode...';
        }
        $this->set('field', $field);
    }

}