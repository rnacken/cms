<?php

class Fieldset_rulesController extends Controller {

    function edit($id = null) {
        $fieldset = $this->Fieldset->getFieldsetById($id); // old data
        // todo: show alternative if no fieldset is found
        if(isset($_POST['submit'])){
            echo 'saved!';
            $fieldset->saveForm($_POST);
        } else {
            echo 'editing mode...';
        }
        $this->set('fieldset', $fieldset);
    }

    function getParams(){
        // todo: check plugins for extra params
    }

    function getValues($paramName, $rule_id = null){
        if ($rule_id){
            settype($rule_id, 'int');
            $fs_rule = new Fieldset_rule(array('id'=>$rule_id));
            $rule = $fs_rule->getById($rule_id, Fieldset_rule::$table_name);
            $value = (is_a($rule, 'Fieldset_rule'))? $rule->value : null;
        } else {
            $value = null;
        }
        // todo: check plugins for extra param-values
        switch($paramName){
            case 'id':
                $this->set('response', '<input type="text" class="form-control" name="value_'.$rule_id.'" value="'.$value.'" />');
                break;
            case 'type':
                $this->set('response', '<input type="text" class="form-control" name="value_'.$rule_id.'" value="'.$value.'" />');
                break;
        }
    }
    function getRuleGroups($fieldset_id){
        $fs_rule = new Fieldset_rule(array('fieldset_id'=>$fieldset_id));
        $fieldset_rules = $fs_rule->getByIndex(array('fieldset_id'=>$fieldset_id), Fieldset_rule::$table_name);
        $result = array();
        $this->set('response', $fieldset_id);
//        foreach($fieldset_rules as $rule){
//            $result[$rule->rule_group][$rule->id] = json_decode(json_encode($rule), true);
//        }
        //$this->set('response', json_encode($result));
    }

}