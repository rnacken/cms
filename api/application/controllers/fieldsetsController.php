<?php

class FieldsetsController extends Controller {

    function edit($id = null) {
        $fieldset = $this->Fieldset->getFieldsetById($id); // old data
        // todo: show alternative if no section is found
        if(isset($_POST['submit'])){
            echo 'saved!';
            $fieldset->saveForm($_POST);
        } else {
            echo 'editing mode...';
        }
        $this->set('fieldset', $fieldset);
    }

}