<?php include(ROOT . "/application/views/" . PARTIALS_DIR . "/header.php"); ?>
<div ng-controller="SectionRuleController">
    <form method="post">
        <button type="submit" name="submit" value="submit">Save</button>
        <hr />
        <h1>Now editing Section: <?=$section->id;?></h1>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-4">
                        <label for="section_title_field" class="control-label">Section title</label>
                        <small class="help-block">Title for this section</small>
                    </div>
                    <div class="col-md-8">
                        <input id="section_title_field"
                            class="form-control"
                            type="text"
                            maxlength="255"
                            value="<?=$section->title;?>"
                            name="title"
                            placeholder="section title"/>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span>Display section if:</span>
            </div>
        </div>
        <?php

        $new_group = 0;
        foreach ($section->rule_groups as $rule_group){
            $current_group = reset($rule_group)->rule_group; ?>
        <div class="rule-groups" >ng-repeat="ruleGroup as ruleGroups">

            <div class="row" >//ng-repeat="rule in ruleGroup">
                <?php foreach ($rule_group as $rule){ ?>
                <div class="col-md-12">
                    <div class="form-group" ng-init="ruleDisabled_<?=$rule->id;?> = false">
                        <div class="col-md-3" ng-init="selectedParameter_<?=$rule->id;?> = getCurrent('<?=$rule->param;?>', 'parameters')">
                            <select
                                id="parameter_field_<?=$rule->id;?>"
                                class="form-control"
                                name="parameter_<?=$rule->id;?>"
                                ng-model="selectedParameter_<?=$rule->id;?>"
                                ng-options="parameter.name group by parameter.group for parameter in parameters track by parameter.val"
                                ng-change="updateValField({{<?=$rule->id;?>}})"
                                ng-disabled="ruleDisabled_<?=$rule->id;?>"
                                >
                            </select>
                        </div>
                        <div class="col-md-3" ng-init="selectedOperator_<?=$rule->id;?> = getCurrent('<?=$rule->operator;?>', 'operators')">
                            <select
                                id="operator_field_<?=$rule->id;?>"
                                class="form-control"
                                name="operator_<?=$rule->id;?>"
                                ng-model="selectedOperator_<?=$rule->id;?>"
                                ng-options="operator.name group by operator.group for operator in operators track by operator.val"
                                ng-disabled="ruleDisabled_<?=$rule->id;?>"
                                >
                            </select>
                        </div>
                        <div class="col-md-3">
                            <div ng-init="value_field_<?=$rule->id;?> = updateValField(<?=$rule->id;?>)"
                                ng-bind-html="value_field_<?=$rule->id;?>">

                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-default" id="add_rule_group" data-rule-group="<?=$rule->rule_group;?>">and</button>
                        </div>
                    </div>
                </div>
                    <?php } ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <span>or</span>
                </div>
            </div>
                <?php
                if ($new_group <= $current_group) $new_group = $current_group+1;
            } ?>
        </div>
        <button class="btn btn-default" id="add_rule_group" data-new-group="<?=$new_group;?>">Add new rule group</button>

    </form>
</div>

<?php include(ROOT . "/application/views/" . PARTIALS_DIR . "/footer.php"); ?>
