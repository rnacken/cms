<?php include(ROOT . "/application/views/" . PARTIALS_DIR . "/header.php"); ?>

<form method="post">
    <button type="submit" name="submit" value="submit">Save</button>
    <hr />
    <h1>Now editing Article: <?=$article->id;?></h1>
<?php

if (isset($sections) && is_array($sections)){
    foreach ($sections as $section){
        foreach($section->fields as $field){
            Template::renderPartial($field);
        }
    }
}
?>

</form>