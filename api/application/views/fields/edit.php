<?php include(ROOT . "/application/views/" . PARTIALS_DIR . "/header.php"); ?>

<form method="post">
    <button type="submit" name="submit" value="submit">Save field</button>
    <hr />
    <h1>Now editing field: <?=$field->id;?></h1>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="maxlength-field">Max length</label>
                <input id="maxlength-field"
                        maxlength="3"
                        name="attrs[maxlength]"
                        value="<?=$field->attrs['maxlength'];?>"
                    />
                <span class="help-block">Maximum characters for this field</span>
            </div>
        </div>
    </div>
    <?php


    ?>

</form>