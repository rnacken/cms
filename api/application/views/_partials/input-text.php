<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="field-<?=$field->id;?>"><?=$field->name;?></label>
            <input id="field-<?=$field->id;?>"
                   type="text"
                   maxlength="<?=$field->attrs['maxlength'];?>"
                   name="<?=$field->name;?>"
                   value="<?=$field->value;?>"
                   class="field-input-text field-<?=$field->name;?> field-id-<?=$field->id;?>" />
        </div>
    </div>
</div>