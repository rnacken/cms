<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CMS-API test</title>
    <link href="<?=SITE_URL;?>bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .center {
            float: none;
            margin-left: auto;
            margin-right: auto;
        }
    </style>

    <script src="<?=SITE_URL;?>bower_components/angular/angular.min.js" async></script>
    <script src="<?=SITE_URL;?>assets/js/app.js" async></script>
<!--    <script src="/assets/js/controllers/sectionRuleController.js" async></script>-->
</head>
<body ng-app>
    <div class="page-header">
        <a href=""><h1>CMS test <small>just testing</small></h1></a>
    </div>
    <div class="container">
