<?php

class Field extends Model {

    public $id;
    public $fieldset_id;
    public $type;
    public $name;
    public $label;
    public $options = array();
    public $options_json = '{}';
    public $attrs = array();
    public $attrs_json = '{}';
    public $sort_number;
    public static $table_name = 'fields';

    // data from field_value
    public $article_id;
    public $index_number;
    public $values = 'deprecated!';

    public function __construct(array $data=array()){
        if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
        if ( isset( $data['fieldset_id'] ) ) $this->fieldset_id = (int) $data['fieldset_id'];
        if ( isset( $data['type'] ) ) $this->type = $data['type'];
        if ( isset( $data['name'] ) ) $this->name = preg_replace ( "/[^a-zA-Z0-9()-]/", "", $data['name'] );
        if ( isset( $data['label'] ) ) $this->label = preg_replace ( "/[^a-zA-Z0-9()-]/", "", $data['label'] );
        if ( isset( $data['options_json'] ) ) $this->options_json = $data['options_json'];
        if ( isset( $data['attrs_json'] ) ) $this->attrs_json = $data['attrs_json'];
        if ( isset( $data['sort_number'] ) ) $this->sort_number = (int) $data['sort_number'];

        // data from field_value (content)
        if ( isset( $data['article_id'] ) ) $this->article_id = (int) $data['article_id'] ;
        if ( isset( $data['index_number'] ) ) $this->index_number = (int) $data['index_number'];
        if ( isset( $data['values'] ) ) $this->values = $data['values'];

        // convert options/attrs to *_json
        if ( isset( $data['options'] ) && is_array( $data['options'] )) $this->options_json = json_encode($data['options']);
        if ( isset( $data['attrs'] ) && is_array( $data['attrs'] )) $this->attrs_json = json_encode($data['attrs']);
        // and vice-versa
        $this->options = json_decode($this->options_json, true);
        $this->attrs = json_decode($this->attrs_json, true);
    }

    public function __destruct(){

    }

    public static function tableFields(){
        return array(
            'id'            => array('type'=>'INT', 'A_I'=>true, 'attr'=>'UNSIGNED'),
            'fieldset_id'   => array('type'=>'INT', 'attr'=>'UNSIGNED'),
            'type'          => array('type'=>'VARCHAR(255)'),
            'name'          => array('type'=>'VARCHAR(255)'),
            'label'         => array('type'=>'VARCHAR(255)'),
            'options_json'  => array('type'=>'TEXT'),
            'attrs_json'    => array('type'=>'TEXT'),
            'sort_number'   => array('type'=>'INT', 'attr'=>'UNSIGNED', 'default'=>0),
        );
    }

    public function saveFieldContent(){
        $data = array('field_id' => $this->id,
            'article_id' => $this->article_id,
            'index_number' => $this->index_number,
            'values' => $this->values
        );
        $field_value = new Field_value($data);
        $field_value->saveFieldContent();
    }

    public function getFieldById($id){
        $result = $this->getByIndex(array('id'=>$id, 'parent_id'=>0), self::$table_name);
        $result->options = json_decode($result->options_json, true);
        $result->attrs = json_decode($result->attrs_json, true);
        // get children

        // update model
        $this->id = $result->id;
        $this->fieldset_id = $result->fieldset_id;
        $this->type = $result->type;
        $this->name = $result->name;
        $this->label = $result->label;
        $this->options = $result->options;
        $this->options_json = $result->options_json;
        $this->attrs = $result->attrs;
        $this->attrs_json = $result->attrs_json;
        $this->sort_number = $result->sort_number;

        return $result;
    }

    public function saveForm($form_data){
        // save
        $options_json = json_encode($form_data['options']);
        $this->save($this->id, array('options_json' => $options_json), self::$table_name);
        foreach($form_data['options'] as $key => $val){
            $this->options[$key] = $val;
        }
        $attrs_json = json_encode($form_data['attrs']);
        $this->save($this->id, array('attrs_json' => $attrs_json), self::$table_name);
        foreach($form_data['attrs'] as $key => $val){
            $this->attrs[$key] = $val;
        }
    }
}