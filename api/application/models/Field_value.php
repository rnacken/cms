<?php

class Field_value extends Model {

    public $id = null;
    public $article_id;
    public $field_id;
    public $index_number;
    public $value;
    public static $table_name = 'field_values';

    private static $index_delimeter = '.';

    public function __construct(array $data=array()){
        if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
        if ( isset( $data['article_id'] ) ) $this->article_id = (int) $data['article_id'];
        if ( isset( $data['field_id'] ) ) $this->field_id = (int) $data['field_id'];
        if ( isset( $data['index_number'] ) ) $this->index_number = $data['index_number'];
        if ( isset( $data['value'] ) ) $this->value = $data['value'];
    }

    public function __destruct(){}

    public static function tableFields(){
        return array(
            'id'            => array('type'=>'INT', 'A_I'=>true, 'attr'=>'UNSIGNED'),
            'article_id'    => array('type'=>'INT', 'attr'=>'UNSIGNED'),
            'field_id'      => array('type'=>'INT', 'attr'=>'UNSIGNED'),
            'index_number'  => array('type'=>'VARCHAR(255)'),
            'value'         => array('type'=>'TEXT')
        );
    }

    public function getFieldValue($article_id, $field, $index_number = '0'){
        $result = array();
        // recursive function to get all values and the children.
        if (!isset($field->children)){

            // no children, stop recursion. pls stahp;
            $child_value_arr = parent::getByIndex(array('article_id'=>$article_id, 'field_id'=>$field->id, 'index_number'=>$index_number), self::$table_name, 'value', false, 1);
            if ($child_value_arr){
                return $child_value_arr[0]->value;
            } else {
                return false;
            }
        }
        foreach($field->children as $child){
            $all_values = parent::getByIndex(array('article_id'=>$article_id, 'field_id'=>$child->id), self::$table_name, 'value, index_number', false);
            if(is_array($all_values) && count($all_values) > 0){
                foreach($all_values as $value){
                    // check if index_number without last part matches current $index_number
                    if(getStrBeforeLast($value->index_number, self::$index_delimeter) == $index_number){
                        $child_value = $this->getFieldValue($article_id, $child, $value->index_number);
                        $result[getStrAfterLast($value->index_number, self::$index_delimeter)][$child->name] = $child_value;
                    }
                }
            }
        }
        return $result;
    }

    public function saveFieldContent(){
        // check if row with article_id & field_id already exists
        $result = $this->getByIndex(array('article_id'=>$this->article_id, 'field_id'=>$this->field_id, 'index_number'=>$this->index_number), self::$table_name, 'id', false, 1);
        if (count($result) == 1){
            $this->id = $result[0]->id;
        }
        $this->save($this->id, (array)$this, self::$table_name);
    }

}