<?php

/**
 * Class to handle articles
 */

class Article extends Model {

    // Properties
    public $id = null;
    public $date_created = null;
    public $type = null;
    public static $table_name = 'articles';

    public function __construct(array $data=array()) {
        if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
        if ( isset( $data['date_created'] ) ) $this->date_created = (int) $data['date_created'];
        if ( isset( $data['type'] ) ) $this->type = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['type'] );
    }

    public static function tableFields(){
        return array(
            'id'            => array('type'=>'INT', 'A_I'=>true, 'attr'=>'UNSIGNED'),
            'date_created'  => array('type'=>'DATETIME'),
            'parent_id'     => array('type'=>'INT', 'default'=>-1),
            'type'          => array('type'=>'VARCHAR(255)')
        );
    }

    public function getArticleById($id){
        $result = $this->getById($id, self::$table_name);

        // update model
        $this->id =             $result->id;
        $this->date_created =   $result->date_created;
        $this->type =           $result->type;

        return $result;
    }

    public function saveForm($form_data){
        $fieldset = new Fieldset();
        $fieldsets = $fieldset->getFieldsets($this);
        foreach($fieldsets as $fieldset){
            if (is_array($fieldset->fields)){    // bruh, do you even contain fields?
                foreach($fieldset->fields as $field){
                    if(array_key_exists($field->name, $form_data)){
                        $field->value = $form_data[$field->name]; // no point
                        $field->saveFieldContent();
                    }
                }
            }
        }
    }
}

?>