<?php

class Fieldset extends Model {

    public $id;
    public $title;
    public $fields = array();
    public static $table_name = 'fieldsets';

    public function __construct(array $data = array()){
        if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
        if ( isset( $data['title'] ) ) $this->title = $data['title'];
    }

    public function __destruct(){

    }


    public static function tableFields(){
        return array(
            'id'             => array('type'=>'INT', 'A_I'=>true, 'attr'=>'UNSIGNED'),
            'title'          => array('type'=>'VARCHAR(255)')
        );
    }

    public function getFieldsetById($id){
        $result = $this->getById($id, self::$table_name);
        $fs_rule = new Fieldset_rule();
        $result->rule_groups = $fs_rule->getRuleGroups($id);
        return $result;
    }

    /** Check which fieldsets should be loaded, with this particular article */
    public function getFieldsets(Article $art = null){
        $fieldset_ids = $this->getIds();
        $fs_rule = new Fieldset_rule();
        $fieldset_ids = $fs_rule->getFieldsets($fieldset_ids, $art);
        $fieldsets = array();
        foreach($fieldset_ids as $fieldset_id){
            $fieldset = $this->getById($fieldset_id, self::$table_name);
            array_push($fieldsets, $fieldset);
        }
//            $fields = $this->getFieldsetFields($fieldset_id, 0, $art);
//            $fieldset->fields = $fields;

//            $field_value = new Field_value();
//            $values = $field_value->getFieldValues($fieldset_is, 0, $art);

        return $fieldsets;
    }

    public function getFieldsetFields($fieldset_id, $parent_id, Article $art){
        // recursive function to get all fields/values and the children of those fields.
        $fields = $this->getByIndex(array('fieldset_id' => $fieldset_id, 'parent_id' => $parent_id), Field::$table_name, '*', 'sort_number');
        // populate fields with names / values
        if (!is_array($fields)) return false;   // end of branch; exit recursive loop
        foreach($fields as &$field){
            $field_value = $this->getByIndex(array('field_id' => $field->id, 'article_id' => $art->id), Field_value::$table_name);
            if($field_value){
                $field->article_id = $field_value[0]->article_id;
                //$field->values[$field_value[0]->index_number] = $field_value[0]->value;
            }
            // enter recursive loop
            $children = $this->getFieldsetFields($fieldset_id, $field->id, $art, $fields);
            if ($children){
                $field->children = $children;
            }
        }
        return $fields;
    }

    public function saveForm($form_data){
        $result = $this->validate($form_data);
        if ($result['success'] == true){
            $this->save($this->id, $form_data, self::$table_name);
        } else {
            var_dump($result['errors']);
        }
    }
}