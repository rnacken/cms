<?php

class Fieldset_rule extends Model {

    public $id;
    public $fieldset_id;
    public $param;
    public $operator;
    public $value;
    public $rule_group;
    public static $table_name = 'fieldset_rules';

    public function __construct(array $data = array()){
        if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
        if ( isset( $data['fieldset_id'] ) ) $this->fieldset_id = (int) $data['fieldset_id'];
        if ( isset( $data['param'] ) ) $this->param = $data['param'];
        if ( isset( $data['operator'] ) ) $this->operator = $data['operator'];
        if ( isset( $data['value'] ) ) $this->value = $data['value'];
        if ( isset( $data['rule_group'] ) ) $this->rule_group = (int) $data['rule_group'];
    }

    public function __destruct(){

    }

    public static function tableFields(){
        return array(
            'id'            => array('type'=>'INT', 'A_I'=>true, 'attr'=>'UNSIGNED'),
            'fieldset_id'   => array('type'=>'INT', 'attr'=>'UNSIGNED'),
            'param'         => array('type'=>'VARCHAR(255)'),
            'operator'      => array('type'=>'CHAR(2)'),
            'value'         => array('type'=>'VARCHAR(255)', 'default'=>'=='),
            'rule_group'    => array('type'=>'INT', 'default'=>-1),
        );
    }

    public function getRuleGroups($fieldset_id){
        $rules = $this->getByIndex(array('fieldset_id'=>$fieldset_id), self::$table_name);
        if (is_array($rules)){
            $rule_groups = array();
            foreach($rules as $rule){
                $rule_groups[$rule->rule_group][$rule->id] = $rule;
            }
            return $rule_groups;
        } else {
            return false;
        }
    }

    /** Check which fieldsets should be loaded, with this particular article */
    public function getFieldsets(array $fieldsets = array(), Article $art = null){
        $fs_rule_groups = array();
        foreach ($fieldsets as $fieldset_id){
            $rules = $this->getByIndex(array('fieldset_id' => $fieldset_id), self::$table_name);
            if (is_array($rules)){
                foreach($rules as $rule){
                    $fs_rule_groups[$rule->fieldset_id][$rule->rule_group][$rule->id] = $rule;
                }
            }
            // if no rules exist, then don't show the fs.
        }
        $result = array();  // array of validated fieldset
        foreach ($fs_rule_groups as $fieldset_id => $rule_groups){
            foreach ($rule_groups as $group_id => $rule_group){
                if ($this->validateRuleGroup($rule_group, $art)){
                    array_push($result, $fieldset_id);
                    break; // don't add this fs again; escape loop
                }
            }
        }
        return $result;
    }

    public function validateRuleGroup(array $rule_group, Article $art = null){
        $valid = true;
        foreach($rule_group as $rule){
            $compare_number = false;
            if (!isset($rule->param) || !isset($rule->operator) || !isset($rule->value)) throw new Exception('missing paramater ('.$rule->param.'), operator ('.$rule->operator.') and/or value ('.$rule->value.') for rule');
            switch($rule->param){
                case 'id':
                    $compare = $art->id;
                    $compare_number = true;
                    break;
                case 'type':
                    $compare = $art->type;
                    break;
                default:
                    // fall back on key
                    if (array_key_exists($rule->param, $art))
                        $compare = $art->$rule->param;
                    else
                        throw new Exception('unknown rule parameter');
            }
            if ($compare_number){
                switch($rule->operator){
                    case '!=':
                        if ($compare == (int)$rule->value) $valid = false;
                        break;
                    case '==':
                    default:
                        if ($compare != (int)$rule->value) $valid = false;
                        break;
                }
            } else {
                switch($rule->operator){
                    case '!=':
                        if ($compare == $rule->value) $valid = false;
                        break;
                    case '==':
                    default:
                        if ($compare != $rule->value) $valid = false;
                        break;
                }
            }

        }
        return $valid;
    }


}