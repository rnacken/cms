var app = angular.module('cmsApp', []);

app.controller('SectionRuleController', function($scope, $http, $sce){
    $scope.parameters = [
        {name:'id', val:'id', group:'article'},
        {name:'type', val:'type', group:'article'}
    ];
    $scope.operators = [
        {name:'==', val:'==', group:'all'},
        {name:'!=', val:'!=', group:'all'},
        {name:'<', val:'<', group:'numbers'},
        {name:'<=', val:'<=', group:'numbers'},
        {name:'>', val:'>', group:'numbers'},
        {name:'>=', val:'>=', group:'numbers'},
        {name:'== (case sensitive)', val:'=c', group:'text'},
        {name:'!= (case sensitive', val:'!c', group:'text'},
        {name:'regex', val:'rx', group:'text'}
    ];

    $scope.ruleGroups = [];

    $scope.getRuleGroups = function(){
        $http.get('/api/section_rules/getRuleGroups/' + $scope.section_id, {}).success(function(data){
            $scope.ruleGroups = data;
            console.log('...'+data);
        });
    }
    $scope.ruleGroups = $scope.getRuleGroups();


    $scope.getCurrent = function(currentVal, valArr){
        if (Object.prototype.toString.call( $scope[valArr] ) === '[object Array]' && $scope[valArr].length > 0){ // is it an array?
            for(var i= 0, iMax=$scope[valArr].length; i<iMax; i++){
                if($scope[valArr][i].val == currentVal) return $scope[valArr][i];
            }
        }
    }
    $scope.updateValField = function(rule_id){
        $scope['ruleDisabled_'+rule_id] = true;
        //currentRule = (newParam)? null : rule_id;
        $http.get('/api/section_rules/getValues/' + $scope['selectedParameter_'+rule_id].val + '/' + rule_id, {}).success(function(data){
            $scope['value_field_'+rule_id] = $sce.trustAsHtml(data);
            $scope['ruleDisabled_'+rule_id] = false;
        });
    }
    $scope.addNewRule = function(){

    }
    $scope.addNewRuleGroup = function(){

    }

});

