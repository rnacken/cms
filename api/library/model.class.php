<?php
class Model extends SQLQuery {
	protected $_model;
    protected $_table;

	function __construct() {

		$this->connect();
		$this->_model = get_class($this);
		$this->_table = strtolower($this->_model)."s";
	}

	function __destruct() {
	}

    function validate($data, $update_model = true){
        $errors = array();
        if (method_exists($this, 'tableFields')){
            foreach($this->tableFields() as $column_name => $column){
                if($update_model){
                    if (!property_exists(get_class($this), $column_name)){ throw new ErrorException('Property '.$column_name.' does not exist in this class'); }
                    if (isset($data[$column_name])){        // did we receive his in the data?
                        $this->$column_name = $data[$column_name];
                    }
                }
                if (array_key_exists($column_name, $data)){                    // only validate if it is a db field
                    $column_type = strtolower($column['type']);

                    if (preg_match('~char.*~', $column_type) || preg_match('~varchar.*~', $column_type)){   // short string
                        $str_length = getBetween($column_type, '(', ')');
                        settype($str_length, 'int');
                        if (strlen($data[$column_name]) > $str_length)
                            $errors[$column_name][] = 'Too many characters (max = '.$str_length.')';
                    } elseif (preg_match('~(tinyint)|(smallint)|(mediumint)|(int)|(bigint)~', $solumn_type)){
                        if (!is_int($data[$column_name]))
                            $errors[$column_name][] = 'Input is not an integer';
                    }
                }
            }
            if (count($errors) > 0){
                return array('success' => false,
                    'errors' => $errors);
            } else {
                return array('success' => true);
            }
        } else {
            throw new ErrorException('Error: could not validate because method tableFields() is missing for model '.get_class($this));
        }

    }



}
