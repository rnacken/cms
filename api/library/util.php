<?php

function getKeyByArrayAttr($attr_name, $attr_value, $array){
    foreach($array as $key => $element){
        if ($element[$attr_name] === $attr_value) return key;
    }
    return false;
}

function issetOr(&$val, $default = null){
    return (isset($val))? $val : $default;
}

function getBetween($str, $start = "", $end = ""){
    $temp1 = strpos($str,$start)+strlen($start);
    $result = substr($str,$temp1,strlen($str));
    $dd = strpos($result, $end);
    if($dd == 0){
        $dd = strlen($result);
    }
    return substr($result, 0, $dd);
}

function getStrAfterLast($str, $delimeter="."){
    return substr($str, strrpos($str, $delimeter) + 1);
}
function getStrBeforeLast($str, $delimeter="."){
    return substr($str, 0, strrpos( $str, $delimeter));
}