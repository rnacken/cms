<?php
class Template {

	protected $variables = array();
	protected $_controller;
	protected $_action;

	function __construct($controller,$action) {
		$this->_controller = $controller;
		$this->_action = $action;
	}

	/** Set Variables */
	function set($name,$value) {
		$this->variables[$name] = $value;
	}

	/** Display Template */
    function render() {
		extract($this->variables);
        if (RESPONSE == 'html'){
            if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_action . '.php')) {
                include (ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_action . '.php');
            }
        } else {
            if (isset($response) && (is_object($response) || is_array($response))){
                echo json_encode($response);
            }
        }

    }

    /** Render partial piece of html */
    function renderPartial($object = null){
        $objectName = strtolower(get_class($object));
        $$objectName = $object;
        if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . PARTIALS_DIR . DS . $object->type . '.php')) {
            include(ROOT . DS . 'application' . DS . 'views' . DS . PARTIALS_DIR . DS . $object->type . '.php');
        }
    }

}
