<?php

class SQLQuery{

    const DEFAULT_PDO_TYPE = PDO::PARAM_STR;

    protected $_conn;
    protected $_result;


    public function connect($dsn = DB_DSN, $username = DB_USERNAME, $password = DB_PASSWORD) {
        if (isset($this->_conn) && $this->_conn instanceof PDO){
            return;     // _conn already made
        }
        $conn = null;
        try {
            $conn = new PDO( $dsn, $username, $password );
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new Exception($e);
        }
        $this->_conn = $conn;
    }

    public function disconnect() {
        $this->_conn = null;
    }

    /** returns an Object */
    public function getById($id, $table = null, $selectFields = '*') {
        $this->connect();
        $table = $this->getTableName($table);
        $selectFields = $this->getSelectFields($table, $selectFields);
        $sql = "SELECT ".$selectFields." FROM ".$table." WHERE id = :id LIMIT 1";
        $st = $this->_conn->prepare( $sql );
        $st->bindValue( ":id", $id, PDO::PARAM_INT );
        $st->execute();
        $row = $st->fetch();
        $model_name = ucfirst(substr($table, 0, -1));
        if ( $row ) return new $model_name( $row );
    }

    /** returns an array of Objects */
    public function getByIndex(array $indexArr, $table = null, $selectFields = '*', $order = false, $limit = false) {
        $this->connect();
        $table = $this->getTableName($table);
        $selectFields = $this->getSelectFields($table, $selectFields);
        $whereQuery = " WHERE ";
        foreach($indexArr as $name => $val){
            $whereQuery .= $name . " = :cmsindex_" . $name . " AND ";
        }
        $whereQuery = substr($whereQuery, 0, -4);
        $orderQuery = (is_string($order))? " ORDER BY ".$order : "";
        $limitQuery = (is_int($limit))? " LIMIT ".strval($limit) : "";
        $sql = "SELECT ".$selectFields." FROM ".$table.$whereQuery.$orderQuery.$limitQuery;
        $st = $this->_conn->prepare( $sql );
        foreach($indexArr as $name => $val){
            $st->bindValue( ":cmsindex_".$name, $val, PDO::PARAM_INT );
        }
        $st->execute();
        $rows = $st->fetchAll(PDO::FETCH_ASSOC);
        if ( $rows ) {
            foreach($rows as &$row){
                $model_name = ucfirst(substr($table, 0, -1));
                $row = new $model_name($row);
            }
            return $rows;
        } else {
            return false;
        }
    }

    /** returns an array with IDs (int) */
    public function getIds($table = null, $order = 'id ASC'){
        $this->connect();
        $table = $this->getTableName($table);
        $sql = "SELECT id FROM ".$table." ORDER BY ".$order;
        $st = $this->_conn->prepare( $sql );
        $st->execute();
        $rows = $st->fetchAll(PDO::FETCH_ASSOC);
        if ( $rows ) {
            $result = array();
            foreach($rows as $row){
                array_push($result, (int)$row['id']);
            }
            return $result;
        } else {
            return false;
        }
    }

    /** use to insert or (if id is already in db) updatex§ */
    public function save($id = null, array $setArr, $table = null){
        $this->connect();
        $table = $this->getTableName($table);
        if ($id !== null){
            // update
            return $this->update($id, $setArr, $table);
        } else {
            // insert
            return $this->insert($id, $setArr, $table);
        }
    }

    /** returns true/false on successful update */
    public function update($id, array $setArr, $table = null){
        $this->connect();
        $table = $this->getTableName($table);
        $tableFields = $this->getTableFields($table);
        $setQuery = '';
        foreach($setArr as $name => $val){
            if (array_key_exists($name, $tableFields)){
                $setQuery .= $name."=:name_".$name.",";
            }
        }
        $setQuery = subStr($setQuery, 0, -1);
        $sql = "UPDATE ".$table." SET ".$setQuery." WHERE id = :id";
        $st = $this->_conn->prepare( $sql );
        foreach($setArr as $name => $val){
            if (array_key_exists($name, $tableFields)){
                $st->bindValue( ":name_".$name, $val, PDO::PARAM_STR );
            }
        }
        $st->bindValue( ":id", $id, PDO::PARAM_INT );
        return $st->execute();
    }

    /** returns new inserted id */
    public function insert($id, array $setArr, $table = null){
        $this->connect();
        $table = $this->getTableName($table);
        $tableFields = $this->getTableFields($table);
        $setQuery1 = '';
        $setQuery2 = '';
        foreach($setArr as $name => $val){
            if (array_key_exists($name, $tableFields)){
                $setQuery1 .= $name.",";
                $setQuery2 .= ":name_".$name.",";
            }
        }
        $setQuery1 = subStr($setQuery1, 0, -1);
        $setQuery2 = subStr($setQuery2, 0, -1);
        $sql = "INSERT INTO ".$table." (".$setQuery1.") VALUES (".$setQuery2.")";
        $st = $this->_conn->prepare( $sql );
        foreach($setArr as $name => $val){
            if (array_key_exists($name, $tableFields)){
                $st->bindValue( ":name_".$name, $val, $this->getPdoType($tableFields[$name]) );
            }
        }
        $result = $st->execute();
        return ($result == false)? false : $this->_conn->lastInsertId();
    }


    /** todo: create PDO style query select method */
    public static function getByQuery( $query = array(), $table = null, $order = 'id ASC' ){

    }
    
    /** Custom SQL Query **/

	function query($query, $singleResult = 0) {
		//$this->_result = mysql_query($query, $this->_dbHandle); // db-handle no longer supported

		if (preg_match("/select/i",$query)) {
            $result = array();
            $table = array();
            $field = array();
            $tempResults = array();
            $numOfFields = mysql_num_fields($this->_result);
            for ($i = 0; $i < $numOfFields; ++$i) {
                array_push($table,mysql_field_table($this->_result, $i));
                array_push($field,mysql_field_name($this->_result, $i));
            }

		
			while ($row = mysql_fetch_row($this->_result)) {
				for ($i = 0;$i < $numOfFields; ++$i) {
					$table[$i] = trim(ucfirst($table[$i]),"s");
					$tempResults[$table[$i]][$field[$i]] = $row[$i];
				}
				if ($singleResult == 1) {
		 			mysql_free_result($this->_result);
					return $tempResults;
				}
				array_push($result,$tempResults);
			}
			mysql_free_result($this->_result);
			return($result);
		}
		

	}

    /** checks if table is public, returns name */
    private function getTableName($table = null){
        if (!$table){
            $table = strtolower(get_class($this)).'s';      // fallback to class that was used
        }
        $className = ucfirst(substr($table, 0, -1));
        if (!class_exists($className, true)){
            throw new Exception('Database error: can\'t check if `'.$table.'` is public; class '.$className.' not found.');
        }
        if (!property_exists($className, 'table_name')){
            throw new Exception('Database error: table `'.$table.'` does not have public access (not defined in class: '.$className.')');
        }
        if ($className::$table_name == $table){
            if (!is_string($table)){
                throw new Exception('Database error: table `'.$table.'` does not have public access.');
            }
            return $table;
        } else {
            throw new Exception('Database error: table `'.$table.'` does not have public access or does not exist.');
        }
    }

    /** returns array with public fields of table */
    private function getTableFields($table = null){
        if (!$table){
            $table = strtolower(get_class($this)).'s';      // fallback to class that was used
        }
        $className = ucfirst(substr($table, 0, -1));
        if (!class_exists($className, true)){
            throw new Exception('Database error: can\'t get fields for `'.$table.'`; class '.$className.' not found!');
        }
        if (!method_exists($className, 'tableFields')){
            return array();         // no tableFields method? return empty array
        }
        return $className::tableFields();
    }

    /** returns string of fields that will be returned from Db after a SELECT */
    private function getSelectFields($table, $selectFields = '*'){
        $allFields = array_keys($this->getTableFields($table));
        $selectFields = ($selectFields==='*')? implode(',', $allFields): $selectFields;
        if (!strlen($selectFields) > 0){
            return $allFields[0];   // return first name, usually 'id'.
        }
        return $selectFields;
    }

    /** returns PDO type for tableFields array */
    private function getPdoType($field){
        if (!is_array($field)) return self::DEFAULT_PDO_TYPE;
        if (isset($field['type'])){
            switch(strtolower($field['type'])){
                case 'tinyint':
                case 'smallint':
                case 'mediumint':
                case 'int':
                case 'bigint':
                    return PDO::PARAM_INT;
                    break;
                default:
                    return self::DEFAULT_PDO_TYPE;
                    break;

            }
        }
    }

}
