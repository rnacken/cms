cmsServices.factory("Articles", ['$http', '$q', function($http, $q){

    var fac = {};

    fac.getArticle = function(articleId){
        var deferred = $q.defer();
        $http.get('/api/articles/view/'+articleId).
            success(function(data, status, headers, config){    // data is returned as json object.
                deferred.resolve(data);
            });
        return deferred.promise;
    }

    return fac;
}]);