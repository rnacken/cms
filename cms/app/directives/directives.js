
// directives
cmsDirectives
//.directive('dynamicForm', ['$interpolate', function($interpolate) {
//    return {
//        priority: 10000,
//        restrict: 'AC',
//        controller: function($scope, $element, $attrs) {
//            $attrs.$set('name', $interpolate($attrs.dynamicForm || $attrs.name)($scope));
//            $element.data('$interpolModelController', null);
//        }
//    };
//}])
.directive('dynamicName', ['$interpolate', function($interpolate) {
    return {
        priority: 10000,
        restrict: 'AC',
        controller: function($scope, $element, $attrs) {
            $attrs.$set('name', $interpolate($attrs.dynamicName || $attrs.name)($scope));
            $element.data('$interpolModelController', null);
        }
    };
}])
.directive('field', ['$http', '$templateCache', '$compile', '$parse', function($http, $templateCache, $compile, $parse) {
    return {
        restrict: 'AE',
        replace: true,
        scope: false,
        replace: true,
        link: function(scope, iElem, iAttrs){

            scope.mainForm = scope.$parent.mainForm;    // check if submitted, etc
            var template;
            var valuePath = (iAttrs.valuepath)?
                iAttrs.valuepath + '.' + scope.field.name :
                scope.valuepath + '.' + scope.field.name;       // get value & path in article.values for this field
            scope.valueArr = $parse(valuePath)(scope);

            if (scope.field.type==='repeater'){
                var repeater = $parse(valuePath)(scope);
                if(angular.isArray(repeater)){
                    if (repeater.length === 0){
                        scope.valueArr = [{}];
                    }
                } else {
                    $parse(valuePath).assign(scope, [{}]);
                    scope.valueArr = [{}];
                }
            } else {
                if(!scope.valueArr){
                    scope.valueArr = null;
                    $parse(valuePath).assign(scope, scope.valueArr);
                }
            }

            var updateField = function(){
                iElem.html(template);
                // if sortable (e.g. repeater); provide model
                var sortableElem = iElem[0].querySelector('.sortable');
                if (sortableElem){ angular.element(sortableElem).attr('ng-model', valuePath); }

                // if input type; provide model
                var cmsField = iElem[0].querySelector('.cms-field') ||
                    iElem[0].querySelector('input') ||
                    iElem[0].querySelector('select') ||
                    iElem[0].querySelector('textarea');
                if (cmsField){
                    angular.element(cmsField).attr('ng-model', valuePath);
                }
                $compile(iElem.contents())(scope);
            }

            scope.addItem = function(modelPath, addBeforeIndex){
                if (addBeforeIndex !== false){  // add before item x
                    addBeforeIndex = parseInt(addBeforeIndex, 10);
                    currentModel = scope.$eval(modelPath.substring(0, modelPath.lastIndexOf('[')));
                    currentModel.splice(addBeforeIndex, 0, {});
                    scope.valueArr = currentModel;
                } else {                            // add empty item at end of array
                    var currentModel = scope.$eval(modelPath+'.'+scope.field.name);
                    currentModel.push({});
                    scope.valueArr = currentModel;
                }
                updateField();
            }
            scope.removeItem = function(modelPath, index){
                index = parseInt(index);
                currentModel = scope.$eval(modelPath.substring(0, modelPath.lastIndexOf('[')));
                if (currentModel.length > 1){       // keep at least one item
                    currentModel.splice(index, 1);
                    updateField();
                }
            }

            scope.$on('fieldsUpdate', function(event, modelPath){
                if (!modelPath || modelPath == valuePath){
                    console.log('updating: '+valuePath);
                    updateField();
                }
            });
            // todo: theming options
            var data = $templateCache.get('field'+scope.field.type);
            if (data){
                template = data;
                updateField();
            } else {
                $http.get('http://cms.localhost/cms/views/fields/'+scope.field.type+'.html').success(function(data){
                    template = data;
                    $templateCache.put('field'+scope.field.type, data);
                    updateField();
                }).error(function(){
                    console.log('template: '+newType+' not found.');
                });
            }
        }
    };
}]);