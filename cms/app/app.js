"use strict";

var cmsApp = angular.module('cmsApp', [
    'ngRoute',
    'ui.sortable',

    'cmsServices',
    'cmsDirectives',
    'articleControllers',
    'cmsControllers'

]);

var articleControllers = angular.module('articleControllers', []);
var cmsControllers = angular.module('cmsControllers', []);

var cmsServices = angular.module('cmsServices', []);
var cmsDirectives = angular.module('cmsDirectives', []);

cmsApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/articles', {
                templateUrl: 'views/articles/article-list.html',
                controller: 'articleListController'
            }).
            when('/articles/view', {
                templateUrl: 'views/articles/article-list.html',
                controller: 'articleListController'             // show list if no id
            }).
            when('/articles/view/:articleId', {
                templateUrl: 'views/articles/article-view.html',
                controller: 'articleViewController'
            }).
            when('/articles/edit', {
                templateUrl: 'views/articles/article-list.html',
                controller: 'articleListController'             // show list if no id
            }).
            when('/articles/edit/:articleId', {
                templateUrl: 'views/articles/article-view.html',
                controller: 'articleViewController'
            }).
            otherwise({
                redirectTo: '/articles'     // list articles as default
            });
    }
]);

cmsApp.filter('typeof', function(){
    return function(input){
        return input + '=' + typeof input;
    };
});