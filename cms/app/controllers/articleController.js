"use strict";

articleControllers.controller('articleListController', ['$scope', '$routeParams', '$http', '$sce',
    function($scope, $routeParams, $http, $sce){
        $scope.test = 'list';
//        if ($routeParams.articleId){
//            $scope.test = $routeParams.articleId;
//        }

    }
]);

articleControllers.controller('articleViewController', ['$scope', '$q', '$routeParams', 'Articles',
    function($scope, $q, $routeParams, Articles){

        $scope.valuepath = 'article.values';
        $scope.article = {};

        if ($routeParams.articleId){
            Articles.getArticle($routeParams.articleId).then(function(data){
                $scope.article = data;
            });
        }

        $scope.submitArticle = function(){
            $scope.mainForm.$dirty = true;
            console.log($scope.article.values);
            //angular.foreach()
        }
    }
]);

articleControllers.controller('articleEditController', ['$scope', '$routeParams', '$http', '$sce',
    function($scope, $routeParams, $http, $sce){
        $scope.test = 'edit all';
        if ($routeParams.articleId){
            $scope.test = 'edit: '+$routeParams.articleId;
        }
    }
]);