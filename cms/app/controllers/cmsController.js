"use strict";

cmsControllers.controller('cmsController', ['$scope', '$http', '$sce',
    function($scope, $http, $sce){

        var tmpList = [];

        $scope.sortableOptions = {
            update: function(event, ui){
                var el = angular.element(event.target.outerHTML);
                el = angular.isArray(el)? el[0].querySelector('.sortable') : el;        // ': el' needed?
                var modelPath = el.attr('ng-model');
                $scope.$broadcast( 'fieldsUpdate', modelPath );
            },
            cursor: 'move',
            containment: 'parent',
            'ui-floating': true,
            placeholder: "ui-sortable-placeholder",
            tolerance: "pointer",
            handle: ".sortable-handle",
            forcePlaceholderSize:true,
            opacity: 0.8,
            items: '.sortable-item'
        };

    }
]);